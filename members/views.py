from django.shortcuts import render

def members(request):
    context = {
        "first_name": "Anjaneyulu",
        "last_name": "Batta",
        "address": "Hyderabad, India"
    }
    template_name="myfirst.html"
    return render(request, template_name, context)